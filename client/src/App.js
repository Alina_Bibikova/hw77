import React, {Component, Fragment} from 'react';
import './App.css';
import Messages from "./containers/Messages/Messages";
import {Route, Switch} from "react-router";
import {Container} from "reactstrap";
import Header from "./components/Header/Header";


class App extends Component {
  render() {
    return (
        <Fragment>
            <Header />
            <Container>
                <Switch>
                  <Route path="/" exact component={Messages} />
                  <Route render={() => <h1>Hot found</h1>} />
                </Switch>
            </Container>
        </Fragment>
    );
  }
}

export default App;
