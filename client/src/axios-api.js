import axios from 'axios';
import {apiURL} from "./constans";

const instance = axios.create({
    baseURL: apiURL
});

export default instance;