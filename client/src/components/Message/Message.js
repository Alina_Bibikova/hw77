import React from 'react';

import './Message.css'
import MessageThumbnail from "../MessageThumbnail/MessageThumbnail";

const Message = props => {
    return (
        <div className="message">
            <MessageThumbnail image={props.image}/>
            <span className="message-author">{props.author}</span>
            <span className="message-text">{props.message}</span>
        </div>
    );
};

export default Message;