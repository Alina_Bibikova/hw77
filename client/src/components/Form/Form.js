import React, {Component} from 'react';

import './Form.css';

class Form extends Component {
    state = {
        author: '',
        message: '',
        image: ''
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData);
        this.setState({
            author: '',
            message: '',
        })
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    fileChangeHandler  = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    render() {
        return (
            <form className="form" onSubmit={this.submitFormHandler}>
                <div className="container">
                    {this.props.error
                        ? <p className="text-warning border-bottom border-warning mb-3">{this.props.error}</p>
                        : null
                    }
                    <div className="form-row">
                        <div className="col-3">
                            <input placeholder="author"
                                   type="text"
                                   className="control"
                                   name="author"
                                   value={this.state.author}
                                   onChange={this.inputChangeHandler}
                            />
                        </div>
                        <div className="col-4">
                            <input placeholder="message"
                                   type="text"
                                   className="control"
                                   name="message"
                                   value={this.state.message}
                                   onChange={this.inputChangeHandler}
                            />
                        </div>
                        <div className="col-4">
                            <input
                                   type="file"
                                   className="control"
                                   name="image"
                                   onChange={this.fileChangeHandler}
                            />
                        </div>

                        <div className="col-1">
                            <button type="submit" className="btn btn-primary">Send</button>
                        </div>
                    </div>
                </div>
            </form>
        );
    }

}

export default Form;
